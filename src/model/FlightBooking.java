package model;

import java.io.Serializable;

public class FlightBooking implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private Flight flight;

	public FlightBooking() {
	}

	public FlightBooking(String name, Flight flight) {
		this.name = name;
		this.flight = flight;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	@Override
	public String toString() {
		return "FlightBooking [id=" + id + ", name=" + name + ", flight=" + flight + "]";
	}
}
