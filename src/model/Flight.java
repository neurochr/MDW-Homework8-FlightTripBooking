package model;

import java.io.Serializable;
import java.util.Date;

public class Flight implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String fromDestination;
	private String toDestination;
	private Date depatureDate;
	private Date arrivalDate;

	public Flight() {
	}

	public Flight(String fromDestination, String toDestination, Date depatureDate, Date arrivalDate) {
		this.fromDestination = fromDestination;
		this.toDestination = toDestination;
		this.depatureDate = depatureDate;
		this.arrivalDate = arrivalDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFromDestination() {
		return fromDestination;
	}

	public void setFromDestination(String fromDestination) {
		this.fromDestination = fromDestination;
	}

	public String getToDestination() {
		return toDestination;
	}

	public void setToDestination(String toDestination) {
		this.toDestination = toDestination;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public String toString() {
		return "Flight [id=" + id + ", fromDestination=" + fromDestination + ", toDestination=" + toDestination
				+ ", depatureDate=" + depatureDate + ", arrivalDate=" + arrivalDate + "]";
	}
}
