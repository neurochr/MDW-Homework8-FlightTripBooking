package model;

import java.io.Serializable;

public class Trip implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String destination;
	private int id;
	private int capacity;
	private int occupied = 0;

	public Trip() {
	}

	public Trip(String destination, int capacity, int occupied) {
		this.destination = destination;
		this.capacity = capacity;
		this.occupied = occupied;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getOccupied() {
		return occupied;
	}

	public void setOccupied(int occupied) {
		this.occupied = occupied;
	}

	@Override
	public String toString() {
		return "Trip [destination=" + destination + ", id=" + id + ", capacity=" + capacity + ", occupied=" + occupied
				+ "]";
	}
}
