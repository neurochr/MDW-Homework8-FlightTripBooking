package model;

import java.io.Serializable;

public class TripBooking implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Trip trip;
	private String name;

	public TripBooking() {
	}

	public TripBooking(String name, Trip trip) {
		this.name = name;
		this.trip = trip;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TripBooking [id=" + id + ", trip=" + trip + ", name=" + name + "]";
	}

}
