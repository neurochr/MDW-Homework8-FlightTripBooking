package service;

import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebService;

import controller.FlightBookingController;
import exception.NotAvailableException;

@WebService
public class FlightBookingService {
	private FlightBookingController controller;

	public FlightBookingService() {
		this.controller = new FlightBookingController();
	}

	public String addFlight(@WebParam(name = "fromDestination") String fromDestination,
			@WebParam(name = "toDestination") String toDestination, @WebParam(name = "departureDate") Date depatureDate,
			@WebParam(name = "arrivalDate") Date arrivalDate) {
		return this.controller.addFlight(fromDestination, toDestination, depatureDate, arrivalDate).toString();
	}

	public String addBooking(@WebParam(name = "name") String name, @WebParam(name = "destination") String destination)
			throws NotAvailableException {
		return this.controller.addBooking(name, destination).toString();
	}
}
