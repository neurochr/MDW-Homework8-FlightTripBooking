package service;

import javax.jws.WebParam;
import javax.jws.WebService;

import controller.TripBookingController;
import exception.NoCapacityException;
import exception.NotAvailableException;

@WebService
public class TripBookingService {
	private TripBookingController controller;

	public TripBookingService() {
		this.controller = new TripBookingController();
	}

	public String addTrip(@WebParam(name = "destination") String destination, @WebParam(name = "capacity") int capacity,
			@WebParam(name = "occupied") int occupied) {
		return this.controller.addTrip(destination, capacity, occupied).toString();
	}

	public String addBooking(@WebParam(name = "name") String name, @WebParam(name = "destination") String destination)
			throws NotAvailableException, NoCapacityException {
		return this.controller.addBooking(name, destination).toString();
	}
}
