package exception;

public class NoCapacityException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoCapacityException(String message) {
		super(message);
	}

}
