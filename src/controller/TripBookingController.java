package controller;

import java.util.Collection;

import db.TripBookingDB;
import exception.NoCapacityException;
import exception.NotAvailableException;
import model.Trip;
import model.TripBooking;

public class TripBookingController {

	public Collection<Trip> getAllTrips() {
		return TripBookingDB.getInstance().getTrips();
	}

	public Trip getTrip(int id) {
		return TripBookingDB.getInstance().getTrip(id);
	}

	public Trip addTrip(Trip trip) {
		return TripBookingDB.getInstance().addTrip(trip);
	}

	public Trip addTrip(String name, int capacity, int occupied) {
		return this.addTrip(new Trip(name, capacity, occupied));
	}

	public TripBooking addBooking(TripBooking booking) {
		return TripBookingDB.getInstance().addBooking(booking);
	}

	public TripBooking addBooking(String name, Trip trip) {
		if (trip.getOccupied() < trip.getCapacity()) {
			TripBooking booking = new TripBooking(name, trip);
			booking.getTrip().setOccupied(booking.getTrip().getOccupied() + 1);
			return this.addBooking(booking);
		} else {
			throw new NoCapacityException("Cannot create booking because trip with id " + trip.getId() + " is full");
		}
	}

	public TripBooking addBooking(String name, int tripId) throws Exception {
		Trip trip = this.getTrip(tripId);
		if (trip != null) {
			return this.addBooking(name, trip);
		} else {
			throw new Exception("Cannot create booking because trip with id " + tripId + " does not exist");
		}
	}

	public TripBooking addBooking(String name, String destination) {
		Trip trip = null;
		for (Trip possTrip : this.getAllTrips()) {
			if (possTrip.getDestination().equals(destination)) {
				trip = possTrip;
			}
		}
		if (trip != null) {
			return this.addBooking(name, trip);
		} else {
			throw new NotAvailableException(
					"Cannot create booking because trip with destination " + destination + " does not exist");
		}
	}

}
