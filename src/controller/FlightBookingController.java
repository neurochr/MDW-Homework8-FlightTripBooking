package controller;

import java.util.Collection;
import java.util.Date;

import db.FlightBookingDB;
import exception.NotAvailableException;
import model.Flight;
import model.FlightBooking;

public class FlightBookingController {

	public Collection<Flight> getAllFlights() {
		return FlightBookingDB.getInstance().getFlights();
	}

	public Flight getFlight(int id) {
		return FlightBookingDB.getInstance().getFlight(id);
	}

	public Flight addFlight(Flight flight) {
		return FlightBookingDB.getInstance().addFlight(flight);
	}

	public Flight addFlight(String fromDestination, String toDestination, Date depatureDate, Date arrivalDate) {
		return this.addFlight(new Flight(fromDestination, toDestination, depatureDate, arrivalDate));
	}

	public Collection<FlightBooking> getAllBookings() {
		return FlightBookingDB.getInstance().getBookings();
	}

	public FlightBooking getBooking(int id) {
		return FlightBookingDB.getInstance().getBooking(id);
	}

	private FlightBooking addBooking(FlightBooking booking) {
		return FlightBookingDB.getInstance().addBooking(booking);
	}

	public FlightBooking addBooking(String name, int flightId) {
		Flight flight = this.getFlight(flightId);
		if (flight != null) {
			return this.addBooking(new FlightBooking(name, flight));
		} else {
			throw new NotAvailableException(
					"Cannot create booking because flight with id " + flightId + " does not exist");
		}
	}

	public FlightBooking addBooking(String name, String destination) {
		Flight flight = null;
		for (Flight possFlight : this.getAllFlights()) {
			if (possFlight.getToDestination().equals(destination)) {
				flight = possFlight;
			}
		}
		if (flight != null) {
			return this.addBooking(new FlightBooking(name, flight));
		} else {
			throw new NotAvailableException(
					"Cannot create booking because flight with destination " + destination + " does not exist");
		}
	}
}
