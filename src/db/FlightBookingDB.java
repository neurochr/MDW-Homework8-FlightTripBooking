package db;

import java.util.Collection;
import java.util.Hashtable;

import model.Flight;
import model.FlightBooking;

public class FlightBookingDB {
	private static FlightBookingDB instance = null;
	private Hashtable<Integer, Flight> flights = new Hashtable<>();
	private Hashtable<Integer, FlightBooking> bookings = new Hashtable<>();
	private int currFlightId = 0;
	private int currBookingId = 0;

	public static FlightBookingDB getInstance() {
		if (instance == null) {
			instance = new FlightBookingDB();
		}
		return instance;
	}

	public Flight addFlight(Flight flight) {
		flight.setId(this.currFlightId++);
		flights.put(flight.getId(), flight);
		return flight;
	}

	public Collection<Flight> getFlights() {
		return flights.values();
	}

	public Flight getFlight(int id) {
		return flights.get(id);
	}

	public Collection<FlightBooking> getBookings() {
		return bookings.values();
	}

	public FlightBooking getBooking(int id) {
		return bookings.get(id);
	}

	public FlightBooking addBooking(FlightBooking booking) {
		booking.setId(currBookingId++);
		bookings.put(booking.getId(), booking);
		return booking;
	}
}
