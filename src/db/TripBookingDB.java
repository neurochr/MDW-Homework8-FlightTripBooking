package db;

import java.util.Collection;
import java.util.Hashtable;

import model.Trip;
import model.TripBooking;

public class TripBookingDB {
	private static TripBookingDB instance = null;
	private Hashtable<Integer, Trip> trips = new Hashtable<Integer, Trip>();
	private Hashtable<Integer, TripBooking> bookings = new Hashtable<>();
	private int currTripId = 0;
	private int currBookingId = 0;

	public static TripBookingDB getInstance() {
		if (instance == null) {
			instance = new TripBookingDB();
		}
		return instance;
	}

	public Trip addTrip(Trip trip) {
		trip.setId(this.currTripId++);
		trips.put(trip.getId(), trip);
		return trip;
	}

	public Collection<Trip> getTrips() {
		return trips.values();
	}

	public Trip getTrip(int id) {
		return trips.get(id);
	}

	public TripBooking addBooking(TripBooking booking) {
		booking.setId(this.currBookingId++);
		this.bookings.put(booking.getTrip().getId(), booking);
		return booking;
	}

	public Collection<TripBooking> getBookings() {
		return bookings.values();
	}
}
